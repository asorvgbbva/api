//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosJSON = require('./movimientosv2.json');
var bodyparser = require('body-parser');

app.use(bodyparser.json());


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/',function(req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
})

app.post('/', function (req, res){
  res.send('Hola recibido su peticion post')
})

app.put('/', function (req, res){
  res.send('Hola recibido su peticion put')
})

app.delete('/', function (req, res){
  res.send('Hola recibido su peticion delete')
})

app.get ('/Clientes/:idCliente',function(req, res) {
  res.send('Aqui tiene al cliene numero ' + req.params.idCliente)
})

app.get ('/Movimientos',function(req, res) {
  res.sendfile('movimientosv1.json')
})

app.get ('/v2/Movimientos',function(req, res) {
  res.json(movimientosJSON)
})

app.get ('/v2/Movimientos/:id',function(req, res) {
  console.log(req.params.id)
  res.send(movimientosJSON[req.params.id - 1])
})

//Petición con Query
app.get ('/v2/Movimientosq/',function(req, res) {
  console.log(req.query)
  res.send('query Recibido ' + req.query)
})

app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send('Movimiento dado de alta V2')
})
